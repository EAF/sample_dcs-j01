package main;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import observer.Observateur;

/*
 * Les librairies import�es ne sont pas compatibles 64bits
 * S'assurer d'avoir un JDK ou une JVM 32 bits
 * */
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;

import socket.CupptSocket;
import thread.ObservableListener;

public class Form1 {

	protected Shell shlCupptSdk;
	protected Display display;

	private Table tableEvents;
	private Text textCUPPSPLT;
	private Text textCUPPSPN;
	private Text textCUPPSOPR;
	private Text textCUPPSCN;
	private Text textCUPPSPP;
	private Text textXSDU;
	private Text textCUPPSUN;
	private Text textCUPPSACN;
	private StyledText styledTextGoals, styledTextXML, styledTextEvent;

	private Button btnInterfacelevelsavailablerequest, btnConnection, btnDisconnect;

	private CupptSocket platformSocket = new CupptSocket();

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Form1 window = new Form1();
			window.open();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlCupptSdk.open();
		shlCupptSdk.layout();
		while (!shlCupptSdk.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();

			}
		}

	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCupptSdk = new Shell();
		shlCupptSdk.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {

				System.out.println("closing...");

			}
		});

		shlCupptSdk.setImage(SWTResourceManager.getImage(Form1.class, "/main/icon53.png"));
		shlCupptSdk.setMinimumSize(new Point(971, 649));
		shlCupptSdk.setSize(760, 432);
		shlCupptSdk.setText("CUPPT SDK - lesson 01 Java");

		Group grpPlatformConnection = new Group(shlCupptSdk, SWT.NONE);
		grpPlatformConnection.setText("Platform Connection");
		grpPlatformConnection.setBounds(10, 10, 935, 105);

		Label lblSitecuppsplt = new Label(grpPlatformConnection, SWT.NONE);
		lblSitecuppsplt.setBounds(10, 22, 101, 15);
		lblSitecuppsplt.setText("Site (CUPPSPLT)");

		textCUPPSPLT = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPLT.setBounds(117, 19, 89, 21);

		Label lblNodecuppspn = new Label(grpPlatformConnection, SWT.NONE);
		lblNodecuppspn.setText("Node (CUPPSPN)");
		lblNodecuppspn.setBounds(212, 22, 101, 15);

		textCUPPSPN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPN.setBounds(319, 19, 89, 21);

		Label lblOpratorcuppsopr = new Label(grpPlatformConnection, SWT.NONE);
		lblOpratorcuppsopr.setText("Operator (CUPPSOPR)");
		lblOpratorcuppsopr.setBounds(414, 22, 123, 15);

		textCUPPSOPR = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSOPR.setBounds(543, 19, 89, 21);

		Label lblComputerNamecuppscn = new Label(grpPlatformConnection, SWT.NONE);
		lblComputerNamecuppscn.setText("Computer Name (CUPPSCN)");
		lblComputerNamecuppscn.setBounds(651, 22, 160, 15);

		textCUPPSCN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSCN.setBounds(817, 19, 108, 21);

		Label lblPortcuppspp = new Label(grpPlatformConnection, SWT.NONE);
		lblPortcuppspp.setText("Port (CUPPSPP)");
		lblPortcuppspp.setBounds(212, 49, 101, 15);

		textCUPPSPP = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPP.setBounds(319, 46, 89, 21);

		Label lblXsdSchema = new Label(grpPlatformConnection, SWT.NONE);
		lblXsdSchema.setText("XSD");
		lblXsdSchema.setBounds(212, 77, 101, 15);

		textXSDU = new Text(grpPlatformConnection, SWT.BORDER);
		textXSDU.setBounds(319, 74, 311, 21);

		Label lblUsernamecuppsun = new Label(grpPlatformConnection, SWT.NONE);
		lblUsernamecuppsun.setText("UserName (CUPPSUN)");
		lblUsernamecuppsun.setBounds(414, 49, 123, 15);

		textCUPPSUN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSUN.setBounds(543, 46, 89, 21);

		Label lblAlternateNamecuppsacn = new Label(grpPlatformConnection, SWT.NONE);
		lblAlternateNamecuppsacn.setText("Alternate Name (CUPPSACN)");
		lblAlternateNamecuppsacn.setBounds(651, 49, 160, 15);

		textCUPPSACN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSACN.setBounds(817, 46, 108, 21);

		TabFolder tabFolder = new TabFolder(shlCupptSdk, SWT.NONE);
		tabFolder.setBounds(10, 121, 658, 336);

		TabItem tabPageLesson1 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson1.setText("Lesson 1");

		Composite composite = new Composite(tabFolder, SWT.NONE);
		tabPageLesson1.setControl(composite);

		btnConnection = new Button(composite, SWT.NONE);
		btnConnection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonConnect_Click(e);
			}
		});
		btnConnection.setAlignment(SWT.LEFT);

		btnConnection.setBounds(443, 36, 197, 25);
		btnConnection.setText("1 - Network Socket Connection");

		btnInterfacelevelsavailablerequest = new Button(composite, SWT.NONE);
		btnInterfacelevelsavailablerequest.setEnabled(false);
		btnInterfacelevelsavailablerequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnInterfacelevelsavailablerequest_Click(e);
			}
		});
		btnInterfacelevelsavailablerequest.setAlignment(SWT.LEFT);
		btnInterfacelevelsavailablerequest.setText("2 - InterfaceLevelsAvailableRequest");
		btnInterfacelevelsavailablerequest.setBounds(443, 67, 197, 25);

		styledTextXML = new StyledText(composite, SWT.BORDER);
		styledTextXML.setBounds(10, 36, 427, 100);
		styledTextXML.setWordWrap(true);

		styledTextEvent = new StyledText(composite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		styledTextEvent.setBounds(10, 142, 630, 156);
		styledTextEvent.setWordWrap(true);
		styledTextEvent.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				styledTextEvent.setTopIndex(styledTextEvent.getLineCount() - 1);
			}
		});

		Label lblXmlMessage = new Label(composite, SWT.NONE);
		lblXmlMessage.setAlignment(SWT.CENTER);
		lblXmlMessage.setForeground(display.getSystemColor(SWT.COLOR_LIST_SELECTION));
		lblXmlMessage.setBounds(10, 15, 427, 15);
		lblXmlMessage.setText("XML Message");

		btnDisconnect = new Button(composite, SWT.NONE);
		btnDisconnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDisconnect_Click(e);
			}
		});
		btnDisconnect.setText("Disconnect");
		btnDisconnect.setBounds(443, 111, 197, 25);

		tableEvents = new Table(shlCupptSdk, SWT.BORDER | SWT.FULL_SELECTION);
		tableEvents.setBounds(10, 463, 658, 138);
		tableEvents.setHeaderVisible(true);
		tableEvents.setLinesVisible(true);

		TableColumn tblclmnTime = new TableColumn(tableEvents, SWT.NONE);
		tblclmnTime.setWidth(186);
		tblclmnTime.setText("Time");

		TableColumn tblclmnEvent = new TableColumn(tableEvents, SWT.NONE);
		tblclmnEvent.setWidth(463);
		tblclmnEvent.setText("Event");

		Group grpLessonGoals = new Group(shlCupptSdk, SWT.NONE);
		grpLessonGoals.setText("Lesson Goals");
		grpLessonGoals.setBounds(674, 136, 271, 465);

		styledTextGoals = new StyledText(grpLessonGoals, SWT.BORDER);
		styledTextGoals.setBackground(SWTResourceManager.getColor(255, 255, 204));
		// styledTextGoals.setBackground(display.getSystemColor(255, 255, 204));
		styledTextGoals.setBounds(10, 23, 251, 400);
		styledTextGoals.setWordWrap(true);

		loadEnvironmentVariables();
		loadGoals();

	}

	// load Environment Variables
	private void loadEnvironmentVariables() {
		try {
			textCUPPSPN.setText(System.getenv("CUPPSPN"));
			textCUPPSPP.setText(System.getenv("CUPPSPP"));

			textCUPPSPLT.setText(System.getenv("CUPPSPLT"));
			textCUPPSCN.setText(System.getenv("CUPPSCN"));
			textCUPPSACN.setText(System.getenv("CUPPSACN"));

			textCUPPSOPR.setText(System.getenv("CUPPSOPR"));
			textCUPPSUN.setText(System.getenv("CUPPSUN"));

			textXSDU.setText(System.getenv("CUPPSXSDU"));
		} catch (Exception ec) {
			dialog("Environment variables are missing.", SWT.ICON_ERROR);
			//tableEventAddLine("Error getting Environment variables:" + ec);
		}

	}

	// load text goals
	private void loadGoals() {

		styledTextGoals.append("1. Read Environment Variables \n");
		styledTextGoals.append("   CUPPSPLT    = Platform IATA code \n");
		styledTextGoals.append("   CUPPSPN     = Platform node to connect to\n");
		styledTextGoals.append("   CUPPSPP     = Node port number\n");
		styledTextGoals.append("   CUPPSCN     = Computername\n");
		styledTextGoals
				.append("   CUPPSACN    = Alternate Computername depending on the Operator\n");
		styledTextGoals.append("\n");
		styledTextGoals.append("   CUPPSOPR    = Operator Code\n");
		styledTextGoals.append("   CUPPSUN     = Username\n");
		styledTextGoals.append("\n");
		styledTextGoals.append("   CUPPSXSDU   = XSD Schema location (UNC)\n");
		styledTextGoals.append("\n");
		styledTextGoals.append("2. Create Network Socket \n");
		styledTextGoals.append("\n");
		styledTextGoals
				.append("3. Sending the initial XML message  : <interfaceLevelsAvailableRequest>  \n");
		styledTextGoals.append("\n");
		styledTextGoals.append("4. Displaying the platform response \n");
	}

	private void buttonConnect_Click(SelectionEvent e) {

		try {
			// if (!socket.isConnected() || socket.isClosed()) {
			if (!platformSocket.getSocket().isConnected() || platformSocket.getSocket().isClosed()) {

				String IP = textCUPPSPN.getText();
				int port = Integer.parseInt(textCUPPSPP.getText());

				platformSocket = new CupptSocket(IP, port, "platform");

				tableEventAddLine("Connected to " + IP + ":" + port);

				// Init ObservableListener
				final ObservableListener observablePlatformListener = new ObservableListener(
						platformSocket);

				platformSocket.setObservableListener(observablePlatformListener);

				// Add observator

				platformSocket.getObservableListener().addObservateur(new Observateur() {
					public void update(String message) {

						final String XML = message;

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {

								CupptSocket socket = observablePlatformListener.getSocket();

								System.out.println();

								styledTextEventAppend('R', XML);

								tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " <-- " + XML);

							}
						});

					}
				});

				btnInterfacelevelsavailablerequest.setEnabled(true);

				generateXML();

			} else {
				dialog("Already connected", SWT.ICON_WARNING);
			}
		} catch (Exception ec) {
			tableEventAddLine("Error connectiong to server:" + ec);

		}

	}

	private void btnInterfacelevelsavailablerequest_Click(SelectionEvent e) {
		try {

			sendXML("interfaceLevelsAvailableRequest");

		} catch (Exception E) {
			System.out.println(E.toString());
		}
	}

	private void sendXML(String XML) {
		sendXML(XML, platformSocket);
	}

	// override function
	private void sendXML(String XML, CupptSocket socket) {

		generateXML(XML);
		// send message
		SendMsg(styledTextXML.getText(), socket);
	}

	private void generateXML(String type) {
		// header
		String XML = "<cupps xmlns=\"http://www.cupps.aero/cupps/01.03\" messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName = \""
				+ type + "\">";

		if (type.equals("interfaceLevelsAvailableRequest")) {
			XML += "<interfaceLevelsAvailableRequest hsXSDVersion=\"{XSDVersion}\"/>";
		} else if (type.equals("interfaceLevelRequest")) {
			XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
		} else if (type.equals("authenticateRequest")) {
			XML += "<authenticateRequest airline=\"{AIRLINE}\" eventToken=\"{APPLICATIONCODE}\" platformDefinedParameter=\"{PLATFORMDEFINEDPARAMETER}\" >";

			XML += "<applicationList>";
			XML += "<application applicationName=\"{APPLICATIONNAME}\" applicationVersion=\"{APPLICATIONVERSION}\" applicationData=\"{APPLICATIONDATA}\"/>";
			XML += "</applicationList>";

			XML += "</authenticateRequest>";
		} else if (type.equals("deviceQueryRequest")) {
			XML += "<deviceQueryRequest deviceName=\"{DEVICENAME}\"/>";
		}

		// end
		XML += "</cupps>";

		styledTextXML.setText(XML);

	}

	private void SendMsg(String XML, CupptSocket socket) {
		try {
			if (XML.contains("{MESSAGEID}"))
				socket.incrementMessageID();

			// replace XML vars
			XML = replace(XML, "{MESSAGEID}", String.valueOf(socket.getMessageID()));
			XML = replace(XML, "{XSDVersion}", "");

			// send header msg
			XML = generateMsgHeader(XML) + XML;

			tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " --> " + XML);

			styledTextEventAppend('S', XML);

			socket.send(XML);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String generateMsgHeader(String XML) throws UnsupportedEncodingException {
		// send header msg
		String version = "01";
		String validCode = "00";
		String size = "" + Integer.toHexString(XML.getBytes("utf-8").length);

		System.out.println("XML Size : " + XML.length() + ", Hex : " + size);

		while (size.length() < 6)
			size = "0" + size;

		return version + validCode + size.toUpperCase();
	}

	private void btnDisconnect_Click(SelectionEvent e) {
		disconnect();

		tableEventAddLine("Disconnected to " + platformSocket.getIP() + ":"
				+ platformSocket.getPort());
	}

	private void clear() {

		styledTextXML.setText("");
		styledTextEvent.setText("");

		btnInterfacelevelsavailablerequest.setEnabled(false);

	}

	/*
	 * Close the Input/Output streams and disconnect not much to do in the catch
	 * clause
	 */
	private void disconnect() {
		try {

			System.out.println("Stop " + platformSocket);

			platformSocket.disconnect();

			clear();

		} catch (Exception e) {
			System.out.println("Exception " + e.getMessage());
		}

	}

	private int dialog(String Message, int OPTIONS) {

		// ICON_ERROR, ICON_INFORMATION, ICON_QUESTION, ICON_WARNING,
		// ICON_WORKING
		MessageBox dialog = new MessageBox(shlCupptSdk, OPTIONS);

		dialog.setText("Airline Application");
		dialog.setMessage(Message);

		return dialog.open();

	}

	private void tableEventAddLine(String text) {

		TableItem item = new TableItem(tableEvents, SWT.NONE);
		item.setText(0, getDate());
		item.setText(1, text);

		tableEvents.select(tableEvents.getItemCount() - 1);
		tableEvents.showSelection();

		System.out.println(text);
	}

	private void styledTextEventAppend(char T, String str) {

		String symbol = (T == 'S') ? "-->" : "<--";

		styledTextEvent.append(T + " " + symbol + " " + getDate() + " :\n");
		styledTextEvent.append(str);
		styledTextEvent.append("\n\r");

	}

	// donne la date avec un format par d�faut
	private String getDate() {
		return getDate("dd/MM/yyyy HH:mm:ss.SSS");
	}

	// surcharge de la fonction pr�c�dente avec le choix du format
	private String getDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}

	private void generateXML() {
		// create request message interfaces
		String XML = "<cupps messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName=\"interfaceLevelsAvailableRequest\">";
		XML += "<interfaceLevelsAvailableRequest hsXsdVersion=\"{XSDVERSION}\"/>";
		XML += "</cupps>";

		styledTextXML.setText(XML);
	}

	private String replace(String originalText, String subStringToFind,
			String subStringToReplaceWith) {
		int s = 0;
		int e = 0;

		StringBuffer newText = new StringBuffer();

		while ((e = originalText.indexOf(subStringToFind, s)) >= 0) {

			newText.append(originalText.substring(s, e));
			newText.append(subStringToReplaceWith);
			s = e + subStringToFind.length();

		}

		newText.append(originalText.substring(s));
		return newText.toString();

	}

}
