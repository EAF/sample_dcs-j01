@echo off
REM d�place vers le JAR et ses lib
cd %~dp0

rem https://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/for.mspx?mfr=true
set $filename=%~n0
set $jar=%$filename%.jar
set $jre=..\jre7\bin

if defined PROGRAMFILES(x86) (set "reg=%windir%\SysWOW64\reg") else set "reg=reg"
set "branch=HKLM\Software\JavaSoft\Java Runtime Environment"

for /f "tokens=3" %%v in ('%reg% query "%branch%" /v "CurrentVersion" ^| find "REG_SZ"') do (
    for /f "tokens=2*" %%I in ('%reg% query "%branch%\%%v" /v "JavaHome" ^| find "REG_SZ"') do (
		set "$jre=%%J\bin"
    )
)

echo launch jre from "%$jre%"
"%$jre%\java" -jar %$jar%

ping 127.0.0.1 -n 5 >nul 
